import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.kevsoft.imagecompare.PdiffImageCompare;

public class PdiffMultiThreaded {

	public static ArrayList<PdiffImageCompare> fillArrayList() {
		ArrayList<PdiffImageCompare> pa = new ArrayList<PdiffImageCompare>();
		PdiffImageCompare pdiff;
		PdiffImageCompare pdiff1;
		PdiffImageCompare pdiff2;

		BufferedImage img1;
		BufferedImage img2;
		BufferedImage img3;

		try {
			img1 = ImageIO.read(new File("testobjekt.jpg"));
			img2 = ImageIO.read(new File("testobjekt1.jpg"));
			img3 = ImageIO.read(new File("testobjekt2.jpg"));

			pdiff = new PdiffImageCompare(img1, img2); // Creates an PdiffImageCompare object with both pictures
			pdiff1 = new PdiffImageCompare(img1, img3);
			pdiff2 = new PdiffImageCompare(img2, img3);
			
			pdiff.setSizeScale(new Dimension(600, 450));
			pdiff1.setSizeScale(new Dimension(600, 450));
			pdiff2.setSizeScale(new Dimension(600, 450)); // Sets both pictures
															// to
															// a certain size

			pa.add(pdiff);
			pa.add(pdiff1);
			pa.add(pdiff2); // Adds the object to an ArrayList
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Reads a picture from it's path

		return pa;
	}

	public static void multiThreadedCompare(ArrayList<PdiffImageCompare> pa) {
		for (int i = 0; i < pa.size(); i++) {
			HashMap<PdiffImageCompare, Double> hm = PdiffImageCompare.comparePercentMultipleParallel(pa); // Puts
																											// the
																											// result
																											// in
																											// a
																											// hash
																											// map
			System.out.println(hm.get(pa.get(i)).doubleValue()); // Gets the
																	// result
		}

	}

	public static void main(String args[]) {
		multiThreadedCompare(fillArrayList());

	}

}
