import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;

import org.kevsoft.imagecompare.PdiffImageCompare;

public class PdiffSingleThreaded {
	public static void singleThreadedCompare(String imgPath1, String imgPath2) {

		PdiffImageCompare pdiff;
		BufferedImage img1;
		BufferedImage img2;
		double result;
		try {
			img1 = ImageIO.read(new File(imgPath1)); // Reads a picture from it's
														// path
			img2 = ImageIO.read(new File(imgPath2));
			pdiff = new PdiffImageCompare(img1, img2); // Creates an pdiff object
														// with both pictures
			pdiff.setSizeScale(new Dimension(600, 450)); // Sets both pictures to
															// a certain size
			result = pdiff.comparePercent(); // Compares each picture's
												// similarity and returns the
												// result in percent
			System.out.println(result);

		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

	}
	public static void main (String args []){
		singleThreadedCompare ("testobjekt.jpg","testobjekt1.jpg"); 
		
	}
	
}
